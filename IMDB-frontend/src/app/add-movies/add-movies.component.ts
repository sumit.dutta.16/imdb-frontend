import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Actors } from '../models/actors.models';
import { Producers } from '../models/producers.model';
import { MoviesService } from '../shared/movies.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { FileUpload } from '../models/fileUpload.models';

import {
  NgbModal,
  ModalDismissReasons,
  NgbModalOptions,
} from '@ng-bootstrap/ng-bootstrap';
import { FileuploadService } from '../shared/fileupload.service';
import { Movies } from '../models/movies.model';

@Component({
  selector: 'app-add-movies',
  templateUrl: './add-movies.component.html',
  styleUrls: ['./add-movies.component.css'],
})
export class AddMoviesComponent implements OnInit {
  public file?: File;
  public actors: Actors[] = [];
  public producers: Producers[] = [];

  public sexArr = ['Male', 'Female', 'Others'];
  percentage = 0;

  currentFileUpload?: FileUpload;
  defaultPoster: string =
    'https://firebasestorage.googleapis.com/v0/b/imdb-imageupload.appspot.com/o/uploads%2Fno-poster-available.jpg?alt=media&token=f655ca13-8862-44df-aa26-e5e813191ae4';

  dropDownList: any = [];
  dropdownSettings: IDropdownSettings = {};
  modalOptions: NgbModalOptions;

  constructor(
    private movieService: MoviesService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private uploadService: FileuploadService
  ) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop',
    };
  }

  ngOnInit(): void {
    console.log(this.file);
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.movieService.getActors().subscribe((response: any) => {
        this.actors = response.data;
        this.dropDownList = this.actors.map((actor: any) => {
          return { item_id: actor.actorid, item_text: actor.name };
        });
      });
      this.movieService.getProducers().subscribe((response: any) => {
        this.producers = response.data;
      });
      this.dropdownSettings = {
        singleSelection: false,
        idField: 'item_id',
        textField: 'item_text',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 6,
        allowSearchFilter: true,
      };
    });
  }

  openActor(content: any) {
    this.modalService.open(content, this.modalOptions).result.then(
      (result) => {
        this.movieService.addActor(result).subscribe((res: any) => {
          if (res.message === 'success') {
            console.log('Actor added');
            this.movieService.getActors().subscribe((response: any) => {
              this.actors = response.data;
              this.dropDownList = this.actors.map((actor: any) => {
                return { item_id: actor.actorid, item_text: actor.name };
              });
            });
          } else {
            console.log('Adding Error');
          }
        });
      },
      (reason) => {
        console.log(reason);
      }
    );
  }

  openProducer(content: any) {
    this.modalService.open(content, this.modalOptions).result.then(
      (result) => {
        this.movieService.addProducer(result).subscribe((res: any) => {
          if (res.message === 'success') {
            console.log('Producer added');
            this.movieService.getProducers().subscribe((response: any) => {
              this.producers = response.data;
            });
          } else {
            console.log('Adding Error');
          }
        });
      },
      (reason) => {
        console.log(reason);
      }
    );
  }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }

  onFileChange(event: any): void {
    this.file = event.target.files[0];
    console.log(this.file);
  }

  addMovie(name: any, year: any, plot: any, actors: any, producer: any) {
    actors = actors.map((element: any) => {
      return element.id;
    });

    if (this.file) {
      const movie = {
        name: name,
        year: year,
        plot: plot,
        actors: actors,
        producer: parseInt(producer),
      };
      this.currentFileUpload = new FileUpload(this.file);
      this.uploadService
        .pushFileToStorage(this.currentFileUpload, movie)
        .subscribe(
          (percentage) => {
            this.percentage = Math.round(percentage ? percentage : 0);
          },
          (error) => {
            console.log(error);
          }
        );
    } else {
      const movie = {
        name: name,
        year: year,
        plot: plot,
        actors: actors,
        producer: parseInt(producer),
        poster: this.defaultPoster,
      };
      this.movieService.addMovie(movie).subscribe((res) => {
        if (res.message === 'success') {
          this.router.navigate(['/']);
        }
      });
    }
  }
}
