import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { catchError, retry } from 'rxjs/operators';

import { Observable, throwError } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FileUpload } from '../models/fileUpload.models';
import { Movies } from '../models/movies.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class FileuploadService {
  private basePath = '/uploads';
  downloadURL?: Observable<string>;

  private serverURL = 'http://localhost:3000/movies';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private http: HttpClient,
    private router: Router
  ) {}

  pushFileToStorage(fileUpload: FileUpload, movieData: any): Observable<any> {
    const filePath = `${this.basePath}/${fileUpload.file.name}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath, fileUpload.file);
    console.log(movieData);

    uploadTask
      .snapshotChanges()
      .pipe(
        finalize(() => {
          storageRef.getDownloadURL().subscribe((downloadURL) => {
            movieData['poster'] = downloadURL;
            console.log(movieData);
            if (movieData.movieid !== undefined) {
              this.http
                .post<any>(
                  this.serverURL + '/updatemovie',
                  movieData,
                  this.httpOptions
                )
                .pipe(retry(1), catchError(this.handleError))
                .subscribe((response) => {
                  if (response.message === 'success') {
                    this.router.navigate(['/']);
                  }
                });
            } else {
              this.http
                .post<any>(
                  this.serverURL + '/addmovie',
                  movieData,
                  this.httpOptions
                )
                .pipe(retry(1), catchError(this.handleError))
                .subscribe((response) => {
                  if (response.message === 'success') {
                    this.router.navigate(['/']);
                  }
                });
            }
            fileUpload.url = downloadURL;
            fileUpload.name = fileUpload.file.name;
            this.saveFileData(fileUpload);
          });
        })
      )
      .subscribe((url) => {
        // if (url !== undefined && url['state'] === 'success') {
        //   // this.http
        //   //   .post<any>(this.serverURL + '/addmovie', this.httpOptions)
        //   //   .pipe(retry(1), catchError(this.handleError));
        // }
      });
    return uploadTask.percentageChanges();
  }

  private saveFileData(fileUpload: FileUpload): void {
    this.db.list(this.basePath).push(fileUpload);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code. // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    } // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
